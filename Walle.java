package robots;

import robocode.*;
import java.awt.Color;

/**
 * Walle - a robot by Berman Schultz
 */
public class Walle extends AdvancedRobot {
	double movieDirect = 1;
	boolean movingForward;

/**
	 * run: Walle's default behavior
	 */
	public void run() {

		while (true) {

			turnRadarRightRadians(360);
			setAdjustRadarForRobotTurn(true);
			setAdjustGunForRobotTurn(true);
			setAdjustRadarForGunTurn(true);
		}

	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		
		double absoluteBearing = getHeading() + e.getBearing();

		double bearingFromGun = robocode.util.Utils.normalRelativeAngleDegrees(absoluteBearing
				- getGunHeading());
		double bearingFromRadar = robocode.util.Utils.normalRelativeAngleDegrees(absoluteBearing
				- getRadarHeading());

		
		if (this.movingForward) {
			setTurnRight(robocode.util.Utils.normalRelativeAngleDegrees(e.getBearing() + 80));
		} else {
			setTurnRight(robocode.util.Utils.normalRelativeAngleDegrees(e.getBearing() + 100));
		}

		if (e.getDistance() > 30) {
			setTurnGunRight(bearingFromGun);
			setTurnRadarRight(bearingFromRadar);
			setBack((e.getDistance() - 140) * movieDirect);

			
			
				fire(3);
			
		}
		else {
			setTurnGunRight(bearingFromGun);
			setTurnRadarRight(bearingFromRadar);
			setBack((e.getDistance() - 140) * movieDirect);
		}
		if (bearingFromGun == 0) {
			scan();
		}
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		movieDirect = -movieDirect;
	}
	

	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		setAhead(100);
	}
	
	public void onWin(WinEvent e){
		while(true){
		setBack(20);
		setTurnGunRight(60);
		setAhead(30);		
}
	}
}
