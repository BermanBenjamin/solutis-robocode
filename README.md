# WALLE ROBOT

## Walle é um Advanced Robot focado em rastrear e perseguir o primeiro inimigo, esquivar do ataques e obter maiores pontuações de tiro, pra conseguir pontuar e ganhar colocações nas batalhas, porém tem como seu ponto fraco a existencia dos outros inimigos, por possui o foco no primeiro alvo, ele se torna vuneravel aos outros integrantes da arena.

### Features

- [x] Rastreio de inimigos 
- [x] Precisão de tiro
- [x] Esquiva ao ser atingido
- [x] Comemoração

## Minha Experiência
<p align="center">Foi uma ótima experiência explorar o robocode, assistir tutoriais e aprender sobre novas coisas que jamais tinha visto, aprender e extrair o maximo de outros robôs da plataforma para conseguir criar um robô eficiente</p>